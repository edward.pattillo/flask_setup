# Basic Flask App Starting Point

This is a very good starting point for a Flask Application.

It has templating established and basic user registration and authentication set up.
It comes bundled with its own Windows virtual environment.
It uses Sqlite3.

Uses:

- Python 3.7.3
- Bootstrap 4.3.1

Dependencies:

Package           Version
----------------- -------
astroid           2.2.5
Click             7.0
colorama          0.4.1
Flask             1.0.3
Flask-WTF         0.14.2
isort             4.3.21
itsdangerous      1.1.0
Jinja2            2.10.1
lazy-object-proxy 1.4.1
MarkupSafe        1.1.1
mccabe            0.6.1
passlib           1.7.1
pip               19.1.1
pylint            2.3.1
setuptools        40.8.0
six               1.12.0
typed-ast         1.4.0
Werkzeug          0.15.4
wrapt             1.11.2
WTForms           2.2.1